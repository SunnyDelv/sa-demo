Australian Government ervices

Authorising a person or organisation

Department of Hhuman S centrelink

Filling in this form Please use black or blue pen.
Print in BLOCK LETTERS

Where you see a box like this

Go to 5 skip to the question

number shown. between.

You do not need to answer the questions in

Only one person or organisation can be requested on this form When you have filled in Part A, make sure Part B is given to your nominee or person permitted to enquire to complete.
If you are affected by family and domestic violence, call 132 850 Monday to Friday, between 8.00 am and 5.00 pm, local time and ask to speak to a departmental social worker.
For more information, go to humanservices.gov.au/domesticviolence

Part A - To be completed by YOU

1 Your Centrelink Reference Number (if known) 4 - l - A

to enquire or act on your behalf

5 Is this request for a person or an organisation?
Tick ONE box only Person Go to next question Organisation Go to 10

6 Your requested person's Centrelink Reference Number (if known) L

Your requested person's name Family name GEORGe

First given name V(LL1An

Second given name PETEK

8 Has requested person been known by any other name(s)?

2 Your name         your                     
                    Include:                 
Family name         name at birth            
6EOK2               name before marriage     
                    previous married name    
First given name    Aboriginal or tribal name
/                   alias                    
Second given name   adoptive name            
Kosc                foster name.             

3 Your date of birth / c|/ 1982

4 Has your permanent or postal address changed since you last told us?
No Go to next question Yes Give details below Your permanent address

No Go to next question Yes Give details below Other name(s)

9 Your requested person's date of birth 01/011100 Go to 11

Postcode Your postal address (if different from above)

Postcode

S$313.1807

HIHHHIIIIIIIHIHIHHHHHHHHHHH CLKOSS313 1807

1of4

10 Your requested organisation's details Trading name of organisation

This is the name of the organisation

not the contact person.

The name of the contact person is to be provided at the end of this question.

Business name of organisation

Australian Business Number (ABN) This is mandatory when nominating an organisation.

Organisation's Centrelink Reference Number (if known)

Organisation's email address

Name of contact person

11 What is your requested person's or organisation's relationship to you (e.g. parent, child, guardian, accountant, Public Trustee)?
R/ Er

12 Your requested person's or organisation's contact details Street address / AWDRews CLoSe Cars

CD Postal address (if different to above)

Postcode cF87.

Postcode

Contact phone number (07) 4003 7[48

13 Please read this before you answer questions 13 to 15 For more information about the different arrangement types, refer to the Notes.
If you have a nominee arrangement of the same type already in place, this request will automatically cancel the existing arrangement Your existing nominee will receive a letter advising that the arrangement has been cancelled at your request.

What arrangement are you requesting?
If you want to request arrangements with more than one person or organisation, you will need to complete a separate form for each one.

Person permitted to enquire

Go to 15

Correspondence nominee Go to 15 Payment nominee Go to 14 BOTH payment and Go to 14 correspondence nominee

14 Give details of the nominee's account where your Centrelink payments are to be paid

It may be easier for your

nominee to manage your payments

by having a separate bank account.
Your nominee must tell us if they change this bank account.

Name of bank, building society or credit union

Branch number (BSB) Account number (this may not be your card number) Account held in the name(s) of

For organisations (if applicable)

only - Group Institution Code

15 What is the reason for making this request?
Voluntary Enduring Power of Attorney Guardianship order Financial management / administration order

Attach a copy of the legal

documents.

16 How long do you want this request to last?
Indefinitely OR From / / To / /

$S313.1807

2of4

Privacy notice

17 You need to read this Privacy and your personal information Your personal information is protected by law (including the PrivacyAct 1988) and is collected by the Australian Government Department of Human Services for the assessment and administration of payments and services.
This information is required to process your application or claim.
Your information may be used by the department, or given

to other parties where you have agreed to that.

or where it

is required or authorised by law (including for the purpose of research or conducting investigations).
You can get more information about the way in which the department will manage your personal information, including our privacy policy, at humanservices.gov.au/privacy

18 If you have a physical or mental disability and are unable to sign this form Go to 19 Your declaration I declare that the information I have provided in this form is complete and correct.

You will need to provide evidence of the person's inability to sign if the arrangement is not court appointed.
Attach a letter from the treating doctor or a Copy of

the medical evidence of the customer' inability to sign this form.

incapacity or

Name of the person signing on behalf of the customer

Relationship to customer Address

Postcode

Contact phone number ( )

Third party declaration I declare that: the information I have provided in this form is complete and

I authorise the person or organisation named on this                                                                       
form, to deal with Centrelink on my behalf according to the   correct.                                                     
arrangement shown on this form.                               the customer is unable to sign this form due to physical or  
                                                              mental disability.                                           
I understand that:                                            it is in the customer's best interest to authorise the person
if my arrangement is voluntary, I can cancel it with          or organisation named on this form, to deal with Centrelink  
Centrelink at any time.                                       on the customer's behalf according to the arrangement        
false or misleading information is a serious offence.         shown on this form.                                          

giving the arrangement may be rejected or cancelled at any time by the Australian Government Department of Human Services, if the person or organisatior is not able to meet their responsibilities and obligations.

Your signature

Signature of the person signing on behalf of the customer

Date

/ /

Date 0/1 0// 2000 oto20

19 Third party authorisation If the customer is unable to sign this form due to physical

20 Which of the following documents are you providing with this form?
Where you are asked to supply documents, attach copies only.
The copies will not be returned.

Tick ALL that apply

or mental disability and the nominee arrangement

is in the

person's best interest, a third party may sign this section on their behalf.

Original photo identification - your nominee or person permitted to enquire is required to take it in person to

For example, an appropriate

third party may be:

one of our service centres

a professional like a treating doctor, nurse, case worker or social worker, or

of Copy the legal document (if required for question 15)

the Enduring Power of Attorney ifit has been made,

or

the person or organisation appointed

by a guardianship

A letter from the treating doctor or a copy of the medical

board, court or tribunal as the customer's administrator.

guardian or

evidence of the this form

customer's incapacity or inability to sign

(if required for question 19)

$5313.1807

Your nominee or person

permitted to enquire

must complete Part B on the next page.

3of4