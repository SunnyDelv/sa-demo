Authorising a person or organisation

Filling in this form * Please use black or blue pen.

* Print in BLOCK LETTERS.

* Where you see a box like this

Go to 5 skip to the question

number shown.You do not need to answer the questions in between.
* Only one person or organisation can be requested on this form.
* When you have flled in Part A, make sure Part B is given to your nominee or person permitted to enquire to complete.
* If you are affected by family and domestic violence, call 132 850 Monday to Friday, between 8.00 am and 5.00 pm, local time and ask to speak to a departmental social worker.
For more information, go towww.

humanservices.gov.au/domesticviolence

Part A - To be completed by YOU

1 Your Centrelink Reference Number (if known)

to enquire or act on your behalf

5 Is this request for a person or an organisation?

Tick ONE box only

Person ☐ Go to next question

Organisation ☐ Goto10

6 Your requested person's Centrelink Reference Number (if known)

7 Your requested person's name Family name

First given name

Second given name

8 Has your requested person been known by any other name(s)?

2 Your name                                    
Family name         Include:                   
                    * name at birth            
                    * name before marriage     
First given name    * previous married name    
                    * Aboriginal or tribal name
                    * alias                    
Second given name   * adoptive name            
                    * foster name.             

3 Your date of birth / /

4 Has your permanent or postal address changed since you last told us?

No ☐ Gotonextquestion

Yes ☐ Givedetailsbelow

Your permanent address

9

Postcode Your postal address (if different from above)

Postcode

SS313.1807

1 of 4

No ☐ Gotonextquestion

Yes ☐ Givedetailsbelow Other name(s)

Your requested person's date of birth

/ /

Go to 11

CLK0SS313 1807

10 Your requested organisation's details

13 Please read this before you answer questions 13 to 15

Trading name of organisation                                    For more information about the different arrangement types,  
This is the name of the organisation, not the contact person.   refer to the Notes.                                          
The name of the contact person is to be provided at the end     If you have a nominee arrangement of the same type already   
of this question.                                               in place, this request will automatically cancel the existing
                                                                arrangement.                                                 
                                                                Your existing nominee will receive a letter advising that the
Business name of organisation                                   arrangement has been cancelled at your request.              
                                                                What arrangement are you requesting?                         
                                                                If you want to request arrangements with more than one       
Australian Business Number (ABN)                                person or organisation, you will need to complete a separate 
This is mandatory when nominating an organisation.              form for each one.                                           

Person permitted to enquire ☐ Go to 15

Organisation's Centrelink Reference Number (if known)

Organisation's email address

@ Name of contact person

11 What is your requested person's or organisation's relationship to you (e.g. parent, child, guardian, accountant, Public Trustee)?

12

Your requested person's or organisation's contact details Street address

Correspondence nominee ☐ Go to 15

Paymentnominee ☐ Goto14

BOTH payment and ☐ Go to 14

correspondence nominee

14 Give details of the nominee's account where your Centrelink payments are to be paid It may be easier for your nominee to manage your payments by having a separate bank account.
Your nominee must tell us if they change this bank account.

Name of bank, building society or credit union

Branch number (BSB)

Account number (this may not be your card number) Account held in the name(s) of

Postcode

Postal address (if different to above)

Postcode

Contact phone number

(

)

For organisations only - Group Institution Code (if applicable)

15 What is the reason for making this request?
Voluntary ☐

Enduring Power of Attorney ☐

Guardianship order ☐

Financial management / administration order ☐

Attach a copy of the legal documents.

16 How long do you want this request to last?
Indefnitely ☐ OR

From / /

To / /

SS313.1807

2 of 4

Privacy notice

17 You need to read this Privacy and your personal information

Your personal information is protected by law (including the Privacy Act 1988) and is collected by the Australian Government Department of Human Services for the assessment and administration of payments and services.
This information is required to process your application or claim.
Your information may be used by the department, or given to other parties where you have agreed to that, or where it is required or authorised by law (including for the purpose of research or conducting investigations).
You can get more information about the way in which the department will manage your personal information, including our privacy policy, atwww.humanservices.gov.au/privacy

18 If you have a physical or mental disability and are unable to sign this form Go to 19

Your declaration

You will need to provide evidence of the person's inability to sign if the arrangement is not court appointed.
Attach a letter from the treating doctor or a copy of the medical evidence of the customer's incapacity or inability to sign this form.

Name of the person signing on behalf of the customer

Relationship to customer

Address

Postcode

Contact phone number

(

)

I declare that the information I have provided in this form is   Third party declaration                                        
complete and correct.                                            I declare that:                                                
I authorise the person or organisation named on this             * the information I have provided in this form is complete and 
form, to deal with Centrelink on my behalf according to the      correct.                                                       
arrangement shown on this form.                                  * the customer is unable to sign this form due to physical or  
                                                                 mental disability.                                             
I understand that:                                               * it is in the customer's best interest to authorise the person
* if my arrangement is voluntary, I can cancel it with           or organisation named on this form, to deal with Centrelink    
Centrelink at any time.                                          on the customer's behalf according to the arrangement          
* giving false or misleading information is a serious offence.   shown on this form.                                            
* the arrangement may be rejected or cancelled at any            Signature of the person signing on behalf of the customer      
time by the Australian Government Department of Human                                                                           
Services, if the person or organisation is not able to meet                                                                     
their responsibilities and obligations.                          On completion of this form,                                    
                                                                 please print and sign by hand                                  

Your signature

On completion of this form, please print and sign by hand

Date

20

/ /

19 Third party authorisation

Go to 20

If the customer is unable to sign this form due to physical or mental disability and the nominee arrangement is in the person's best interest, a third party may sign this section on their behalf.
For example, an appropriate third party may be: * a professional like a treating doctor, nurse, case worker or social worker, or

* the Enduring Power of Attorney if it has been made, or

* the person or organisation appointed by a guardianship board, court or tribunal as the customer's guardian or administrator.

Tick ALL that                                                                                                                                                   apply
Original photo identifcation - your nominee or person permitted to enquire is required to take it in person to one of our service centres                       ☐    
Copy of the legal document (if required for question 15)                                                                                                        ☐    
A letter from the treating doctor or a copy of the medical evidence of the customer's incapacity or inability to sign this form (if required for question 19)   ☐    

SS313.1807

3 of 4

Date

/ /

Which of the following documents are you providing with this form?
Where you are asked to supply documents, attach copies only.
The copies will not be returned.

Your nominee or person permitted to enquire must complete Part B on the next page.

Part B - To be completed by your NOMINEE or PERSON       Returning your form                                                
PERMITTED TO ENQUIRE                                     You can return this form and any supporting documents:             
                                                         * online (excluding identity documents) using your Centrelink      
21 Provideapassword                                      online account. For more information, go towww                     
We will ask this password every time you contact us.                                                                        
The password needs to have 4 to 12 letters or numbers.   humanservices.gov.au/submitdocumentsonline                         
                                                         * in person at one of our service centres, if you are unable to use
                                                         your Centrelink online account.                                    

Privacy notice

22 You need to read this Privacy and your personal information

Your personal information is protected by law (including the Privacy Act 1988) and is collected by the Australian Government Department of Human Services for the assessment and administration of payments and services.
This information is required to process your application or claim.
Your information may be used by the department, or given to other parties where you have agreed to that, or where it is required or authorised by law (including for the purpose of research or conducting investigations).
You can get more information about the way in which the department will manage your personal information, including our privacy policy, atwww.humanservices.gov.au/privacy

23 Acceptance by nominee or person permitted to enquire

Make sure your personal and/or organisation details are correct in Part A.

For more information about your obligations as a nominee or person permitted to enquire, refer to the Notes.

I declare that I understand and accept the responsibilities and obligations for the arrangement requested in this form.

I understand that:

* any personal information I am given access to under this arrangement is protected under Commonwealth legislation.
I agree to access, use or disclose the information only as authorised by the person to whom the information relates.
* my appointment as a nominee or person permitted to enquire may be revoked or suspended by the Australian Government Department of Human Services if I do not comply with my responsibilities and obligations.
* giving false or misleading information is a serious offence.

Signature of the nominee or person permitted to enquire

On completion of this form, please print and sign by hand

Date / /

.

If you are outside Australia, you can:

* post to: Department of Human Services International Services PO Box 7809 CANBERRA BC ACT 2610 Australia

* faxto: +61362222799

SS313.1807

4 of 4